
import os
import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC


import datetime, xlrd

driver_path='Drivers//chromedriver.exe'
def open_browser(driver_path=driver_path):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--disable-infobars")
    chrome_options.add_argument("--disable-logging")
    chrome_options.add_argument("--disable-login-animations")
    chrome_options.add_argument("--disable-notifications")
    chrome_options.add_argument("--disable-default-apps")
    chrome_options.add_argument("--log-level=3")
    return webdriver.Chrome(options=chrome_options,executable_path = driver_path )
	

def send_report_sp1(driver_path=driver_path,dict_data={},tutor="",browser=None):
    '''
    dict={
        'Grupo':'S1',
        'codigo_integrantes: [1234,456,789,123,0,0],
        'nota': [1,1,1,1,0,0],
        'all_integrante':[(1234,1),(456,1),(789,1),(123,1),(0,0),(0,0)],
    }
    '''
    dict_data_temp={
        'Grupo':'S1',
        'codigo_integrantes': [1234,456,789,123,0,0],
        'Equipo':'GRUPO 1',
        'SP1':{
        'nota': [1,1,1,1,0,0],
        'all_integrante':[(1234,1),(456,1),(789,1),(123,1),(0,0),(0,0)],
        }
    }
    
    browser.maximize_window()
    browser.get('https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdURFFCVjRKQjlGSk9ZTFNaWVdJM0NKS0hDVy4u')
    time.sleep(2)
    wait = WebDriverWait(browser, 10)
    fecha = browser.find_element_by_xpath('//input[@aria-labelledby="QuestionId_r76f9ecfc332340789b7e26dff2507025 QuestionInfo_r76f9ecfc332340789b7e26dff2507025"]')
    fecha = browser.find_element_by_xpath('//input[1]')
    fecha.send_keys(xlrd.xldate.xldate_as_datetime(dict_data['SP3']['FSP'], 0).strftime("%d/%m/%Y"))
    if tutor=="":
        browser.find_element_by_xpath('//input[@value="MIGUEL ANGEL NORIEGA CHARRIS"]').click()
    else:
        browser.find_element_by_xpath('//input[@value="'+tutor+'"]').click()

    grupo = browser.find_element_by_xpath('//input[@aria-labelledby="QuestionId_rabaa60b77bd3465e94108df18b34cd0d QuestionInfo_rabaa60b77bd3465e94108df18b34cd0d"]')
    grupo.send_keys(dict_data['Grupo'])
    equipo=browser.find_element_by_xpath('//input[@value="'+dict_data['Equipo']+'"]').click()
    #os.system('pause')
    btnSiguiente=browser.find_element_by_xpath('//button[@aria-label="Siguiente"]').click()
    wait = WebDriverWait(browser, 5)
    integrantes_txt=["QuestionId_r75d7d6a6070745b2a2e524ffc7d69402 QuestionInfo_r75d7d6a6070745b2a2e524ffc7d69402",
    "QuestionId_r2d4d71480ead4321b0118871784b5c59 QuestionInfo_r2d4d71480ead4321b0118871784b5c59",
    "QuestionId_r84cdb564073d4ba7a71e2283231abf39 QuestionInfo_r84cdb564073d4ba7a71e2283231abf39",
    "QuestionId_rbff44bce3c89432d80a92f3900557768 QuestionInfo_rbff44bce3c89432d80a92f3900557768",
    "QuestionId_r4c58321cf81840b5b663e401d54b6297 QuestionInfo_r4c58321cf81840b5b663e401d54b6297",
    "QuestionId_r3218dec9102f453a89561a956b47966c QuestionInfo_r3218dec9102f453a89561a956b47966c"]
    text_ev=['No evidenciado','Evidenciado']
    for id,inte_txt in enumerate(integrantes_txt):
        browser.find_element_by_xpath('//input[@aria-labelledby="'+inte_txt+'"]').send_keys(dict_data['SP1']['all_integrante'][id][0])
        browser.find_element_by_xpath('//input[@aria-label="Integrante '+str(id+1)+', '+text_ev[dict_data['SP1']['all_integrante'][id][1]]+'"]').click()
    #os.system('pause')

    btnSiguiente=browser.find_element_by_xpath('//button[@aria-label="Siguiente"]').click()
    wait = WebDriverWait(browser, 5)
    btnChecks=["r0fec66a27fa4449783e83ace3a63c1fe", #objetivo y la justificación
    "rcfc30609fb254ba9a0b1d7cd3b6b37fa", #los roles
    "r0293979a639741e8a45d3e0c8ab7f032", #objetivos y la misión
    "r640d731a6ed1474b8808e3a343c9acb6", # los requerimientos funcionales y no funcionales del sistema
    "r81e10f7f23eb4fcfa6020df136849982" #Reuniones
    ]
    for btn_txt in btnChecks:
        browser.find_element_by_xpath('//input[@name="'+btn_txt+'"][@value="Evidenciado"]').click()
    browser.find_element_by_xpath('//textarea[@aria-labelledby="QuestionId_r5fe32032b1e742f088f0744f76f86434 QuestionInfo_r5fe32032b1e742f088f0744f76f86434"]').send_keys('Se concretaron reuniones, correos y mensajes de whatsapp')
    
    #btnEnviar=browser.find_element_by_xpath('//button[@title="Enviar"]/div[1]').click()
    browser.find_element_by_xpath('//button[@title="Enviar"]/div[1]').click()
    time.sleep(2)
    ''' 
    print("Finish")
    '''

def send_report_sp2(driver_path=driver_path,dict_data={},tutor="",browser=None):
    '''
    dict={
        'Grupo':'S1',
        'codigo_integrantes: [1234,456,789,123,0,0],
        'nota': [1,1,1,1,0,0],
        'all_integrante':[(1234,1),(456,1),(789,1),(123,1),(0,0),(0,0)],
    }
    '''
    dict_data_temp={
        'Grupo':'S1',
        'codigo_integrantes': [1234,456,789,123,0,0],
        'Equipo':'GRUPO 1',
        'SP1':{
        'nota': [1,1,1,1,0,0],
        'all_integrante':[(1234,1),(456,1),(789,1),(123,1),(0,0),(0,0)],
        }
    }
    
    browser.maximize_window()
    browser.get('https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUM1JLRFJWSDZHNDYzVk8wSkFEMkdOWVNWMS4u')
    time.sleep(2)
    wait = WebDriverWait(browser, 10)
    #fecha = browser.find_element_by_xpath('//input[@aria-labelledby="QuestionId_r76f9ecfc332340789b7e26dff2507025 QuestionInfo_r76f9ecfc332340789b7e26dff2507025"]')
    fecha = browser.find_element_by_xpath('//input[1]')
    fecha.send_keys(xlrd.xldate.xldate_as_datetime(dict_data['SP2']['FSP'], 0).strftime("%d/%m/%Y"))
    if tutor=="":
        browser.find_element_by_xpath('//input[@value="MIGUEL ANGEL NORIEGA CHARRIS"]').click()
    else:
        browser.find_element_by_xpath('//input[@value="'+tutor+'"]').click()
    grupo = browser.find_element_by_xpath('//input[@aria-labelledby="QuestionId_rabaa60b77bd3465e94108df18b34cd0d QuestionInfo_rabaa60b77bd3465e94108df18b34cd0d"]')
    grupo.send_keys(dict_data['Grupo'])
    equipo=browser.find_elements_by_xpath('//input[@value="'+dict_data['Equipo']+'"]')
    if len(equipo)>0:
        equipo[0].click()
    else:
        equipo=browser.find_element_by_xpath('//input[@value="'+dict_data['Equipo'].replace(' ', '  ')+'"]').click()
    #os.system('pause')
    btnSiguiente=browser.find_element_by_xpath('//button[@aria-label="Siguiente"]').click()
    wait = WebDriverWait(browser, 5)
    browser.find_element_by_xpath('//input[@name="r75c936c32acf4f0091067ba1d6a69b0b"]').click() #Click Sprint 2
    integrantes_txt=["QuestionId_r75d7d6a6070745b2a2e524ffc7d69402 QuestionInfo_r75d7d6a6070745b2a2e524ffc7d69402",
    "QuestionId_r2d4d71480ead4321b0118871784b5c59 QuestionInfo_r2d4d71480ead4321b0118871784b5c59",
    "QuestionId_r84cdb564073d4ba7a71e2283231abf39 QuestionInfo_r84cdb564073d4ba7a71e2283231abf39",
    "QuestionId_rbff44bce3c89432d80a92f3900557768 QuestionInfo_rbff44bce3c89432d80a92f3900557768",
    "QuestionId_r4c58321cf81840b5b663e401d54b6297 QuestionInfo_r4c58321cf81840b5b663e401d54b6297",
    "QuestionId_r3218dec9102f453a89561a956b47966c QuestionInfo_r3218dec9102f453a89561a956b47966c"]
    text_ev=['No evidenciado','Evidenciado']
    for id,inte_txt in enumerate(integrantes_txt):
        browser.find_element_by_xpath('//input[@aria-labelledby="'+inte_txt+'"]').send_keys(dict_data['SP2']['all_integrante'][id][0])
        browser.find_element_by_xpath('//input[@aria-label="Integrante '+str(id+1)+', '+text_ev[dict_data['SP2']['all_integrante'][id][1]]+'"]').click()
    #os.system('pause')

    btnSiguiente=browser.find_element_by_xpath('//button[@aria-label="Siguiente"]').click()
    wait = WebDriverWait(browser, 5)
    btnChecks=["r0fec66a27fa4449783e83ace3a63c1fe", #objetivo y la justificación
    "rcfc30609fb254ba9a0b1d7cd3b6b37fa", #los roles
    "r0293979a639741e8a45d3e0c8ab7f032", #objetivos y la misión
    "r640d731a6ed1474b8808e3a343c9acb6", # los requerimientos funcionales y no funcionales del sistema
    "r81e10f7f23eb4fcfa6020df136849982", #Reuniones
    "r59dd0e99db95493f8c1abd51c36124a7"
    ]
    for btn_txt in btnChecks:
        browser.find_element_by_xpath('//input[@name="'+btn_txt+'"][@value="Evidenciado"]').click()
    browser.find_element_by_xpath('//textarea[@aria-labelledby="QuestionId_r5fe32032b1e742f088f0744f76f86434 QuestionInfo_r5fe32032b1e742f088f0744f76f86434"]').send_keys('Se concretaron reuniones, correos y mensajes de whatsapp')
    #os.system('pause')
    #btnEnviar=browser.find_element_by_xpath('//button[@title="Enviar"]').click()
    browser.find_element_by_xpath('//button[@title="Enviar"]/div[1]').click()
    time.sleep(2)
    #os.system('pause')
    
    #print("")


def send_report_sp3(driver_path=driver_path,dict_data={},tutor="",browser=None):
    '''
    dict={
        'Grupo':'S1',
        'codigo_integrantes: [1234,456,789,123,0,0],
        'nota': [1,1,1,1,0,0],
        'all_integrante':[(1234,1),(456,1),(789,1),(123,1),(0,0),(0,0)],
    }
    '''
    dict_data_temp={
        'Grupo':'S1',
        'codigo_integrantes': [1234,456,789,123,0,0],
        'Equipo':'GRUPO 1',
        'SP1':{
        'nota': [1,1,1,1,0,0],
        'all_integrante':[(1234,1),(456,1),(789,1),(123,1),(0,0),(0,0)],
        }
    }
    
    browser.maximize_window()
    browser.get('https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUN0RaOTFFQ0FLWFY0UDkzNks3M0VHUTcwNi4u')
    time.sleep(2)
    wait = WebDriverWait(browser, 10)
    #fecha = browser.find_element_by_xpath('//input[@aria-labelledby="QuestionId_r76f9ecfc332340789b7e26dff2507025 QuestionInfo_r76f9ecfc332340789b7e26dff2507025"]')
    fecha = browser.find_element_by_xpath('//input[1]')
    fecha.send_keys(xlrd.xldate.xldate_as_datetime(dict_data['SP3']['FSP'], 0).strftime("%d/%m/%Y"))
    if tutor=="":
        browser.find_element_by_xpath('//input[@value="MIGUEL ANGEL NORIEGA CHARRIS"]').click()
    else:
        browser.find_element_by_xpath('//input[@value="'+tutor+'"]').click()
    grupo = browser.find_element_by_xpath('//input[@aria-labelledby="QuestionId_rabaa60b77bd3465e94108df18b34cd0d QuestionInfo_rabaa60b77bd3465e94108df18b34cd0d"]')
    grupo.send_keys(dict_data['Grupo'])
    equipo=browser.find_elements_by_xpath('//input[@value="'+dict_data['Equipo']+'"]')
    if len(equipo)>0:
        equipo[0].click()
    else:
        equipo=browser.find_element_by_xpath('//input[@value="'+dict_data['Equipo'].replace(' ', '  ')+'"]').click()
    #os.system('pause')
    btnSiguiente=browser.find_element_by_xpath('//button[@aria-label="Siguiente"]').click()
    wait = WebDriverWait(browser, 5)
    browser.find_element_by_xpath('//input[@name="r75c936c32acf4f0091067ba1d6a69b0b"]').click() #Click Sprint 2
    integrantes_txt=["QuestionId_r75d7d6a6070745b2a2e524ffc7d69402 QuestionInfo_r75d7d6a6070745b2a2e524ffc7d69402",
    "QuestionId_r2d4d71480ead4321b0118871784b5c59 QuestionInfo_r2d4d71480ead4321b0118871784b5c59",
    "QuestionId_r84cdb564073d4ba7a71e2283231abf39 QuestionInfo_r84cdb564073d4ba7a71e2283231abf39",
    "QuestionId_rbff44bce3c89432d80a92f3900557768 QuestionInfo_rbff44bce3c89432d80a92f3900557768",
    "QuestionId_r4c58321cf81840b5b663e401d54b6297 QuestionInfo_r4c58321cf81840b5b663e401d54b6297",
    "QuestionId_r3218dec9102f453a89561a956b47966c QuestionInfo_r3218dec9102f453a89561a956b47966c"]
    text_ev=['No evidenciado','Evidenciado']
    for id,inte_txt in enumerate(integrantes_txt):
        browser.find_element_by_xpath('//input[@aria-labelledby="'+inte_txt+'"]').send_keys(dict_data['SP3']['all_integrante'][id][0])
        browser.find_element_by_xpath('//input[@aria-label="Integrante '+str(id+1)+', '+text_ev[dict_data['SP3']['all_integrante'][id][1]]+'"]').click()
    #os.system('pause')

    btnSiguiente=browser.find_element_by_xpath('//button[@aria-label="Siguiente"]').click()
    wait = WebDriverWait(browser, 5)
    btnChecks=["r0fec66a27fa4449783e83ace3a63c1fe", #objetivo y la justificación
    "rcfc30609fb254ba9a0b1d7cd3b6b37fa", #los roles
    "r0293979a639741e8a45d3e0c8ab7f032", #objetivos y la misión
    "r640d731a6ed1474b8808e3a343c9acb6", # los requerimientos funcionales y no funcionales del sistema
    "r81e10f7f23eb4fcfa6020df136849982", #Reuniones
    "r59dd0e99db95493f8c1abd51c36124a7"
    ]
    for btn_txt in btnChecks:
        browser.find_element_by_xpath('//input[@name="'+btn_txt+'"][@value="Evidenciado"]').click()
    browser.find_element_by_xpath('//textarea[@aria-labelledby="QuestionId_r5fe32032b1e742f088f0744f76f86434 QuestionInfo_r5fe32032b1e742f088f0744f76f86434"]').send_keys('Se concretaron reuniones, correos y mensajes de whatsapp')
    browser.find_element_by_xpath('//button[@title="Enviar"]/div[1]').click()
    time.sleep(2)

def return_value_xls(sheet,j,colum_titles_num,string):
	return_value=sheet.cell_value(j, colum_titles_num[string])
	if type(return_value)==str:
		return return_value
	else:
		return int(return_value)

def read_xls(file_excel):
    loc = (file_excel)   
    wb = xlrd.open_workbook(loc) 
    sheet = wb.sheet_by_index(0)
    colum_titles=['COD','GRUP','EQP','SP1','SP2','SP3','FSP1','FSP2','FSP3'] #TODO: Add SP4, SP5
    titles_num=[]
    colum_names=[]
    for i in range(sheet.ncols): 
        colum_names.append(sheet.cell_value(0, i))
    for title in colum_titles:
        if title in colum_names:
            titles_num.append(colum_names.index(title))
        else:
            print('Error: No se encuentra el dato %s'% title)
            break
    colum_titles_num=dict(zip(colum_titles,titles_num))

    def FSP_FILL(dict_temp={},numSP=5,sheet=None,j=None,colum_titles_num=None):
        for i in range(1,numSP+1):
            dict_temp['SP'+str(i)]['FSP']=return_value_xls(sheet,j,colum_titles_num,'FSP'+str(i))
        return dict_temp

    def dict_temp_empty(grup,eqp,sp_temp):
        dict_temp={
            'Grupo':grup,
            'Equipo':eqp,
            'codigo_integrantes': [0,0,0,0,0,0],
            'SP1': sp_temp,
            'SP2': sp_temp,
            'SP3': sp_temp,
            'SP4': sp_temp,
            'SP5': sp_temp,
            }
        return dict_temp
    GRUP_TEMP=None
    EQP_TEMP=None
    SP_TEMP={
        'FSP':'',
        'nota': [0,0,0,0,0,0],
        'all_integrante':[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
        }
    all_reports=[]
    print(sheet.nrows)
    for j in range(1,sheet.nrows):
        if (GRUP_TEMP==None and EQP_TEMP==None) or GRUP_TEMP!=return_value_xls(sheet,j,colum_titles_num,'GRUP'):
            if GRUP_TEMP!=return_value_xls(sheet,j,colum_titles_num,'GRUP') and EQP_TEMP!=None:
                all_reports.append(dict_temp)
            GRUP_TEMP=return_value_xls(sheet,j,colum_titles_num,'GRUP')
            EQP_TEMP=return_value_xls(sheet,j,colum_titles_num,'EQP')
            dict_temp=dict_temp_empty(GRUP_TEMP,EQP_TEMP,SP_TEMP)
            dict_temp=FSP_FILL(dict_temp=dict_temp,numSP=3,sheet=sheet,j=j,colum_titles_num=colum_titles_num) #TODO: numSP=5
            num_int=0
        elif GRUP_TEMP==return_value_xls(sheet,j,colum_titles_num,'GRUP') and EQP_TEMP!=return_value_xls(sheet,j,colum_titles_num,'EQP'):
            all_reports.append(dict_temp)
            EQP_TEMP=return_value_xls(sheet,j,colum_titles_num,'EQP')
            dict_temp=dict_temp_empty(GRUP_TEMP,EQP_TEMP,SP_TEMP)
            dict_temp=FSP_FILL(dict_temp=dict_temp,numSP=3,sheet=sheet,j=j,colum_titles_num=colum_titles_num) #numSP=5
            num_int=0
        #print(num_int,j)
        dict_temp['codigo_integrantes'][num_int]=return_value_xls(sheet,j,colum_titles_num,'COD')
        dict_temp['SP1']['nota'][num_int]=return_value_xls(sheet,j,colum_titles_num,'SP1')
        dict_temp['SP1']['all_integrante'][num_int]=[dict_temp['codigo_integrantes'][num_int],dict_temp['SP1']['nota'][num_int]]
        dict_temp['SP2']['nota'][num_int]=return_value_xls(sheet,j,colum_titles_num,'SP2')
        dict_temp['SP2']['all_integrante'][num_int]=[dict_temp['codigo_integrantes'][num_int],dict_temp['SP2']['nota'][num_int]]
        dict_temp['SP3']['nota'][num_int]=return_value_xls(sheet,j,colum_titles_num,'SP3')
        dict_temp['SP3']['all_integrante'][num_int]=[dict_temp['codigo_integrantes'][num_int],dict_temp['SP3']['nota'][num_int]]
        #TODO:Add SP4 y SP5
        num_int+=1
    all_reports.append(dict_temp)
    #print(len(all_report))
    #print(all_report)
    return all_reports

def send_reports_sp1(file_excel,tutor=""):
    browser=open_browser()
    all_reports=read_xls(file_excel)
    for report in all_reports:
        send_report_sp1(browser=browser,dict_data=report, tutor=tutor)

def send_reports_sp2(file_excel,tutor=""):
    browser=open_browser()
    all_reports=read_xls(file_excel)
    for id,report in enumerate(all_reports):
        send_report_sp2(browser=browser,dict_data=report,tutor=tutor)
        print(id, report['Grupo'],report['Equipo'])

def send_reports_sp3(file_excel, tutor=""):
    browser=open_browser()
    all_reports=read_xls(file_excel)
    for id,report in enumerate(all_reports):
        send_report_sp3(browser=browser,dict_data=report,tutor=tutor)
        print(id, report['Grupo'],report['Equipo'])

'''
SP5 https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUN0JON1A1SjhUMkFQUkw2MEg3STIxTk1QUy4u
SP4 https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUQjJZQTRZVjJJSzlYMjRGRkcyME0wRjJXSS4u
SP3 https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUN0RaOTFFQ0FLWFY0UDkzNks3M0VHUTcwNi4u
SP2 https://forms.office.com/Pages/ResponsePage.aspx?id=Mq2EpfbcUEGktX-v-SSKpsRsJmBtuPFHqPO9acA1UZdUM1JLRFJWSDZHNDYzVk8wSkFEMkdOWVNWMS4u

'''
if __name__=='__main__':
    #browser=open_browser()
    #send_report(browser=browser)
    #read_xls('data2.xls')
    send_reports_sp1(file_excel='data2.xls',tutor="MIGUEL ANGEL NORIEGA CHARRIS")
    send_reports_sp2(file_excel='data2.xls',tutor="MIGUEL ANGEL NORIEGA CHARRIS")
    send_reports_sp3(file_excel='data2.xls',tutor="MIGUEL ANGEL NORIEGA CHARRIS")